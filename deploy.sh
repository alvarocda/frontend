sshpass -e ssh -o StrictHostKeyChecking=no root@$HOST_DEV << ENDSSH
  docker network create container-lan || true
  echo "Baixando Imagem do Frontend"
  docker pull $IMAGE
  docker rm --force $CI_PROJECT_NAME || true
  echo "Iniciando container Frontend"
  docker run -d -it -p 8090:80 --network container-lan --name $CI_PROJECT_NAME $IMAGE
ENDSSH
