import { NgModule } from '@angular/core';

import { HomepageComponent } from './containers/homepage.component';
import { RouterModule } from '@angular/router';
import { routes } from './homepage.routes';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ConfirmaExclusaoComponent } from './components/confirma-exclusao/confirma-exclusao.component';
import { PessoaModalComponent } from './components/pessoa-modal/pessoa-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatTooltipModule,
    MatPaginatorModule,
    ConfirmaExclusaoComponent,
    MatProgressSpinnerModule,
    PessoaModalComponent,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  exports: [],
  declarations: [HomepageComponent],
  providers: [],
})
export class HomepageModule {}
