import { Component, OnDestroy, OnInit } from '@angular/core';
import { IConsultaPessoa, IPaginacao, IPessoa } from '../homepage.types';
import { Subject, takeUntil } from 'rxjs';
import { HomepageFacade } from '../homepage.facade';
import { PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'homepage',
  templateUrl: 'homepage.component.html',
})
export class HomepageComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<any> = new Subject<any>();
  displayedColumns: string[] = ['name', 'acoes'];
  pessoasPaginacao!: IPaginacao;
  consultandoPessoas: boolean = true;
  formBusca!: FormGroup;
  constructor(
    private _homepageFacade: HomepageFacade,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this._homepageFacade.pessoas$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((pessoas: IPaginacao) => (this.pessoasPaginacao = pessoas));
    this._homepageFacade.listandoPessoas$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((listando: boolean) => (this.consultandoPessoas = listando));
    this._homepageFacade.listarPessoas({} as IConsultaPessoa);

    this.criarFormFiltro();
  }

  buscar(): void {
    const parametros: IConsultaPessoa = {
      nome: this.formBusca.value['nome'],
      cpf: this.formBusca.value['cpf'],
    };
    parametros.cpf = parametros.cpf.replaceAll('.', '').replaceAll('-', '');
    this._homepageFacade.listarPessoas(parametros);
  }

  criarFormFiltro(): void {
    this.formBusca = this._fb.group({
      nome: new FormControl(''),
      cpf: new FormControl(''),
    });
  }

  cadastrarPessoa(): void {
    this._homepageFacade.cadastrarPessoa();
  }
  deletarPessoa(pessoa: IPessoa) {
    this._homepageFacade.deletarPessoa(pessoa);
  }
  visualizarPessoa(pessoa: IPessoa) {
    this._homepageFacade.visualizarPessoa(pessoa);
  }
  editarPessoa(pessoa: IPessoa) {
    this._homepageFacade.editarPessoa(pessoa);
  }
  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

  handlePagination(event: PageEvent) {
    this._homepageFacade.handlePagination(event);
  }
}
