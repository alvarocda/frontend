import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  IConsultaPessoa,
  IControlePaginacao,
  IPaginacao,
} from '../homepage.types';

@Injectable({ providedIn: 'root' })
export class HomepageState {
  private _pessoas$: BehaviorSubject<IPaginacao>;
  private _listandoPessoas$: BehaviorSubject<boolean>;
  private _consultaPessoaParametros$: BehaviorSubject<IConsultaPessoa>;
  private _controlePaginacao$: BehaviorSubject<IControlePaginacao>;
  constructor() {
    this._pessoas$ = new BehaviorSubject({} as IPaginacao);
    this._listandoPessoas$ = new BehaviorSubject(false);
    this._controlePaginacao$ = new BehaviorSubject({ page: 0, size: 10 });
    this._consultaPessoaParametros$ = new BehaviorSubject(
      {} as IConsultaPessoa
    );
  }

  get pessoas$(): Observable<IPaginacao> {
    return this._pessoas$.asObservable();
  }

  get pessoas(): IPaginacao {
    return this._pessoas$.value;
  }

  set pessoas(pessoas: IPaginacao) {
    this._pessoas$.next(pessoas);
  }

  get listandoPessoas$(): Observable<boolean> {
    return this._listandoPessoas$.asObservable();
  }

  get listandoPessoas(): boolean {
    return this._listandoPessoas$.value;
  }

  set listandoPessoas(listando: boolean) {
    this._listandoPessoas$.next(listando);
  }

  get consultaPessoaParametros$(): Observable<IConsultaPessoa> {
    return this._consultaPessoaParametros$.asObservable();
  }

  get consultaPessoaParametros(): IConsultaPessoa {
    return this._consultaPessoaParametros$.value;
  }

  set consultaPessoaParametros(parametros: IConsultaPessoa) {
    this._consultaPessoaParametros$.next(parametros);
  }

  get controlePaginacao$(): Observable<IControlePaginacao> {
    return this._controlePaginacao$.asObservable();
  }

  get controlePaginacao(): IControlePaginacao {
    return this._controlePaginacao$.value;
  }

  set controlePaginacao(paginacao: IControlePaginacao) {
    this._controlePaginacao$.next(paginacao);
  }
}
