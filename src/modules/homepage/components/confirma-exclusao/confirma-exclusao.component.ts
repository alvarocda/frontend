import { Component, OnInit, inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle,
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { IPessoa } from '../../homepage.types';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'confirma-exclusao',
  templateUrl: 'confirma-exclusao.component.html',
  standalone: true,
  imports: [
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    CommonModule,
  ],
})
export class ConfirmaExclusaoComponent {
  readonly dialogRef = inject(MatDialogRef<ConfirmaExclusaoComponent>);
  readonly data = inject<IPessoa>(MAT_DIALOG_DATA);

  confirmaExclusao() {
    console.log('Confirmou exclusão');
    this.dialogRef.close(true);
  }
}
