import { Component, OnInit, inject } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle,
} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { IAcaoPessoaModal, IPessoa } from '../../homepage.types';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { NgxMaskDirective } from 'ngx-mask';
import { ReactiveFormsModule } from '@angular/forms';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
@Component({
  selector: 'modal-pessoa',
  templateUrl: 'pessoa-modal.component.html',
  standalone: true,
  imports: [
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    NgxMaskDirective,
  ],
})
export class PessoaModalComponent implements OnInit {
  readonly dialogRef = inject(MatDialogRef<PessoaModalComponent>);
  readonly data = inject<IAcaoPessoaModal>(MAT_DIALOG_DATA);
  formData!: FormGroup;

  constructor(private _fb: FormBuilder) {}
  ngOnInit(): void {
    if (this.data.acao == 'cadastrar') {
      this.criarFormularioCadastro();
    } else if (this.data.acao == 'visualizar') {
      this.criarFormularioVisualizacao();
    } else {
      this.criarFormularioAlteracao();
    }
  }

  criarFormularioCadastro(): void {
    this.formData = this._fb.group({
      nome: new FormControl('', [Validators.required]),
      telefone: new FormControl('', [Validators.required]),
      dataNascimento: new FormControl('', [Validators.required]),
      cpf: new FormControl('', [Validators.required]),
    });
  }

  criarFormularioAlteracao(): void {
    this.formData = this._fb.group({
      nome: new FormControl(this.data.pessoa?.nome, [Validators.required]),
      telefone: new FormControl(this.data.pessoa?.telefone, [
        Validators.required,
      ]),
      dataNascimento: new FormControl(this.data.pessoa?.dataNascimento, [
        Validators.required,
      ]),
      cpf: new FormControl(this.data.pessoa?.cpf, [Validators.required]),
    });
  }

  criarFormularioVisualizacao(): void {
    this.formData = this._fb.group({
      nome: new FormControl({ value: this.data.pessoa?.nome, disabled: true }),
      telefone: new FormControl({
        value: this.data.pessoa?.telefone,
        disabled: true,
      }),
      dataNascimento: new FormControl({
        value: this.data.pessoa?.dataNascimento,
        disabled: true,
      }),
      cpf: new FormControl({ value: this.data.pessoa?.cpf, disabled: true }),
    });
  }

  salvar() {
    const pessoa: IPessoa = this.formData.value;
    this.dialogRef.close(pessoa);
  }
}
