import { Injectable } from '@angular/core';
import { HomepageState } from './state/homepage.state';
import { Observable } from 'rxjs';
import {
  IConsultaPessoa,
  IControlePaginacao,
  IPaginacao,
  IPessoa,
} from './homepage.types';
import { HomepageService } from './services/homepage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Dialog } from '@angular/cdk/dialog';
import { ConfirmaExclusaoComponent } from './components/confirma-exclusao/confirma-exclusao.component';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from '../../core/services/alert.service';
import { PageEvent } from '@angular/material/paginator';
import { PessoaModalComponent } from './components/pessoa-modal/pessoa-modal.component';

@Injectable({ providedIn: 'root' })
export class HomepageFacade {
  pessoas$: Observable<IPaginacao>;
  listandoPessoas$: Observable<boolean>;
  consultaParametros$: Observable<IConsultaPessoa>;
  controlePaginacao$: Observable<IControlePaginacao>;
  constructor(
    private _homepageState: HomepageState,
    private _homepageServce: HomepageService,
    private _matDialog: MatDialog,
    private _alertService: AlertService
  ) {
    this.pessoas$ = this._homepageState.pessoas$;
    this.listandoPessoas$ = this._homepageState.listandoPessoas$;
    this.consultaParametros$ = this._homepageState.consultaPessoaParametros$;
    this.controlePaginacao$ = this._homepageState.controlePaginacao$;
  }

  listarPessoas(parametros: IConsultaPessoa) {
    this._homepageState.consultaPessoaParametros = parametros;
    this._homepageState.listandoPessoas = true;
    this._homepageServce
      .listarPessoas(parametros, this._homepageState.controlePaginacao)
      .subscribe({
        next: (pessoas: IPaginacao) => {
          this._homepageState.pessoas = pessoas;
          this._homepageState.listandoPessoas = false;
        },
        error: (err: HttpErrorResponse) => {
          this._homepageState.listandoPessoas = false;
        },
      });
  }

  visualizarPessoa(pessoa: IPessoa): void {
    const dialogRef = this._matDialog.open(PessoaModalComponent, {
      data: { pessoa, acao: 'visualizar' },
    });
  }

  editarPessoa(pessoa: IPessoa): void {
    const dialogRef = this._matDialog.open(PessoaModalComponent, {
      data: { pessoa, acao: 'editar' },
    });
    dialogRef.afterClosed().subscribe((pessoaAlterada: IPessoa) => {
      if (pessoaAlterada) {
        pessoaAlterada.id = pessoa.id;
        this._homepageServce.alterarPessoa(pessoaAlterada).subscribe({
          next: (_) => {
            this._alertService.success('Cadastro alterado com sucesso');
            this.listarPessoas(this._homepageState.consultaPessoaParametros);
          },
          error: (err: HttpErrorResponse) => {
            this._alertService.error(
              `Erro ${err.status} - Falha ao alterar cadastro. Consulta o log do navegador para mais detalhes.`
            );
            console.error(err);
          },
        });
      }
    });
  }

  cadastrarPessoa() {
    const dialogRef = this._matDialog.open(PessoaModalComponent, {
      data: { acao: 'cadastrar' },
    });
    dialogRef.afterClosed().subscribe((pessoa) => {
      if (pessoa) {
        this._homepageServce.cadastrarPessoa(pessoa).subscribe({
          next: (_) => {
            this._alertService.success('Pessoa cadastrada com sucesso');
            this.listarPessoas(this._homepageState.consultaPessoaParametros);
          },
          error: (err: HttpErrorResponse) => {
            this._alertService.error(
              `Erro ${err.status} - Falha ao cadastrar pessoa. Consulte o log do navegador para mais detalhes`
            );
            console.error(err);
          },
        });
      }
    });
  }

  handlePagination(event: PageEvent) {
    this._homepageState.controlePaginacao.size = event.pageSize;
    this._homepageState.controlePaginacao.page = event.pageIndex;
    this.listarPessoas(this._homepageState.consultaPessoaParametros);
  }
  deletarPessoa(pessoa: IPessoa): void {
    const dialogRef = this._matDialog.open(ConfirmaExclusaoComponent, {
      data: pessoa,
    });
    dialogRef.afterClosed().subscribe((confirma: boolean) => {
      console.log('Confirmou? -> ' + confirma);
      if (confirma) {
        this._homepageServce.excluirPessoa(pessoa.id).subscribe({
          next: (_) => {
            this._alertService.success('Pessoa excluida com sucesso');
            this.listarPessoas(this._homepageState.consultaPessoaParametros);
          },
          error: (err: HttpErrorResponse) => {
            this._alertService.success(
              `Erro ${err.status} - Falha ao excluir pessoa. Consule o console para mais detalhes`
            );
            console.error(err);
          },
        });
      }
    });
  }
}
