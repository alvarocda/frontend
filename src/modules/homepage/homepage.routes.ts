import { Routes } from '@angular/router';
import { HomepageComponent } from './containers/homepage.component';

export const routes: Routes = [
    {
        path: '',
        component: HomepageComponent,
    }
]