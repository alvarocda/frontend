import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  IConsultaPessoa,
  IControlePaginacao,
  IPaginacao,
  IPessoa,
} from '../homepage.types';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HomepageService {
  baseUrl: string = 'https://cicd-mongo-api.alvarocda.dev';
  constructor(private httpClient: HttpClient) {}

  listarPessoas(
    parametrosConsulta: IConsultaPessoa,
    paginacao: IControlePaginacao
  ): Observable<IPaginacao> {
    const url = `${this.baseUrl}/pessoas`;

    const params = new HttpParams()
      .set('page', paginacao.page)
      .set('size', paginacao.size)
      .set('cpf', parametrosConsulta.cpf ?? '')
      .set('nome', parametrosConsulta.nome ?? '');
    return this.httpClient.get<IPaginacao>(url, { params: params });
  }

  cadastrarPessoa(usuario: IPessoa) {
    const url = `${this.baseUrl}/pessoas`;
    return this.httpClient.post(url, usuario);
  }

  excluirPessoa(id: string) {
    const url = `${this.baseUrl}/pessoas/${id}`;
    return this.httpClient.delete(url);
  }

  alterarPessoa(pessoa: IPessoa) {
    const url = `${this.baseUrl}/pessoas/${pessoa.id}`;
    return this.httpClient.put(url, pessoa);
  }

  detalharPessoa(id: string): Observable<IPessoa> {
    const url = `${this.baseUrl}/pessoas/${id}`;
    return this.httpClient.get<IPessoa>(url);
  }
}
