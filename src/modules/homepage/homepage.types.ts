export interface IPaginacao {
  count: number;
  page: number;
  pageSize: number;
  totalPages: number;
  content: IPessoa[];
}

export interface IPessoa {
  id: string;
  nome: string;
  telefone: string;
  dataNascimento: string;
  cpf: string;
  enderecos: IEndereco[];
}

export interface IEndereco {
  logradouro: string;
  numero: string;
  complemento: string;
  bairro: string;
  cidade: ICidade;
}

export interface ICidade {
  nome: string;
  estado: string;
}

export interface IConsultaPessoa {
  cpf: string;
  nome: string;
}

export interface IControlePaginacao {
  page: number;
  size: number;
}

export interface IAcaoPessoaModal {
  acao: string;
  pessoa?: IPessoa;
}
